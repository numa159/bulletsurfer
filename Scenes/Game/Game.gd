extends Node2D

var frame = 0

func _ready():
	randomize()
	get_node("PlayerControl/Player").connect("player_died", self, "game_over")
	$PlayerControl/Player.connect("enemie_despawned", $Entities, "spawn_enemie")

func game_over():
	get_tree().change_scene("res://Scenes/Game/GameOver.tscn")

func _process(delta):
	frame += 1
	if frame == (randi() % 700) :
		$Entities.spawn_enemie()
		if GlobalVars.score > 5 and GlobalVars.buff_count <= 0:
			$Entities.spawn_buff()
	if frame > 699:
		frame = 0