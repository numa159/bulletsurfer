extends Node2D

const enemie = preload("res://Engine/Enemie.tscn")
var explode_eff = preload("res://Engine/ExplodeEffect.tscn")

const MAX_BUFFS = 1
enum Buffs {GREEN}
var buffs = {
	GREEN : preload("res://Engine/GreenBuff.tscn")
}
var buff_colors = {
	GREEN : Color(0,1,0)
}

func _ready():
	spawn_enemie()

func spawn_enemie():
	var i = 0
	var j = 0
	while i == 0:
		i = (randi() % 3 - 1)
	while j == 0:
		j = (randi() % 3 - 1)
	var new_enemie = enemie.instance()
	var pos = GlobalVars.player_pos + Vector2(((randi() % 500) + 200) * j, ((randi() % 500) + 200) * i)
	new_enemie.position = pos
	add_child(new_enemie)

func spawn_buff():
	print("buff spawned")
	var i = 0
	var j = 0
	while i == 0:
		i = (randi() % 3 - 1)
	while j == 0:
		j = (randi() % 3 - 1)
	var buff_idx = randi() % MAX_BUFFS
	var buff = buffs[buff_idx].instance()
	var pos = GlobalVars.player_pos + Vector2(((randi() % 500) + 200) * j, ((randi() % 500) + 200) * i)
	buff.position = pos
	GlobalVars.buff_count += 1
	add_child(buff)

func enemie_dead(enemie_pos):
	var eff = explode_eff.instance()
	eff.modulate = Color(1,0,0)
	eff.position = enemie_pos
	add_child(eff)
	eff.emitting = true
	var tween = Tween.new()
	tween.interpolate_callback(eff, 1.2, "queue_free")
	tween.start()
	spawn_enemie()
	spawn_enemie()

func buff_consumed(buff_type, buff_pos):
	var eff = explode_eff.instance()
	eff.modulate = buff_colors[buff_type - 1]
	eff.position = buff_pos
	add_child(eff)
	eff.emitting = true
	var tween = Tween.new()
	tween.interpolate_callback(eff, 1.2, "queue_free")
	tween.start()
