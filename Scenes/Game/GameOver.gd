extends Control

onready var score_label = get_node("Container/HBoxContainer/Score")
onready var hs_label = get_node("Container/HBoxContainer/HScore")
onready var new_hs = get_node("Container/NewHS")

func _ready():
	score_label.text = str("SCORE: ", GlobalVars.score)
	hs_label.text = str("HIGH SCORE: ", GlobalVars.high_score)
	if GlobalVars.score > GlobalVars.high_score:
		score_label.set("custom_colors/font_color", Color(0,1,0))
		GlobalVars.high_score = GlobalVars.score
		new_hs.show()
		GlobalVars.save_hs()
	elif GlobalVars.score == GlobalVars.high_score:
		score_label.set("custom_colors/font_color", Color(0,1,0))
	reset_values()

func reset_values():
	GlobalVars.enemie_count = 0
	GlobalVars.current_enemies = 1
	GlobalVars.score = 0
	GlobalVars.player_pos = Vector2()
	GlobalVars.buff_count = 0

func _input(event):
	if (event is InputEventScreenTouch or event is InputEventKey or event is InputEventMouseButton) and event.is_pressed():
		get_tree().change_scene("res://Scenes/Game/Game.tscn")