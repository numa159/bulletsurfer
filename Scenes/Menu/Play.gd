extends TextureButton

func _unhandled_key_input(event):
	if event is InputEventKey:
		if event.scancode != 16777217:
			get_tree().change_scene("res://Scenes/Game/Game.tscn")

func _on_Play_pressed():
	get_tree().change_scene("res://Scenes/Game/Game.tscn")

func _on_Play_gui_input(ev):
	if ev is InputEventScreenTouch and ev.is_pressed():
		get_tree().change_scene("res://Scenes/Game/Game.tscn")