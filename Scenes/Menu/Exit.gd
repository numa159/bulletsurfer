extends TextureButton

func _ready():
	if GlobalVars.operating_system == "Android":
		hide()

func _on_Exit_pressed():
	get_tree().quit()
