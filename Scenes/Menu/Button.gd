extends Button

func _on_Button_pressed():
	GlobalVars.reset_hs()
	self.queue_free()

func _on_Button_gui_input(ev):
	if ev is InputEventScreenTouch and ev.is_pressed():
		GlobalVars.reset_hs()
		self.queue_free()