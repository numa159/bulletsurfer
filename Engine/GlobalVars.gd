extends Node

const save_path = "user://.score"

var player_pos = Vector2(0,0)
var high_score = 0
var score = 0
var enemie_count = 0
var current_enemies = 1
var player
var buff_count = 0
var operating_system
var game_over = false

func _init():
	operating_system = OS.get_name()
	load_hs()

func save_hs():
	print("high score saved!")
	var save_file = File.new()
	save_file.open(save_path, File.WRITE)
	save_file.store_32(high_score)
	save_file.close()

func load_hs():
	var save_file = File.new()
	if not save_file.file_exists(save_path):
		print("score file not found!")
		return
	print("high score loaded!")
	save_file.open(save_path, File.READ)
	high_score = save_file.get_32()
	save_file.close()

func reset_hs():
	print("high score reset!")
	var save_file = File.new()
	save_file.open(save_path, File.WRITE)
	save_file.store_32(0)
	save_file.close()
	load_hs()
