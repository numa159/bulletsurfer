extends KinematicBody2D

const TYPE = "Buff"
const BUFF_TYPE = 1 # G_BUFF
const BUFF_DURATION = 5

func die():
	GlobalVars.buff_count -= 1
	get_parent().buff_consumed(BUFF_TYPE, self.position)
	queue_free()