extends KinematicBody2D

var direction = Vector2(1,0)
var speed = 4.5
var following = false
const TYPE = "Bullet"

func _physics_process(delta):
	if following:
		self.direction = self.position - GlobalVars.player_pos
	self.position -= direction.normalized() * speed
