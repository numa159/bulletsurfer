extends KinematicBody2D

var direction
const SPEED = 3
const TYPE = "Enemie"
var frames_since_bullet = 0

var bullet = preload("res://Engine/Bullet.tscn")

func _init():
	GlobalVars.enemie_count += 1

func _physics_process(delta):
	follow_player()
	frames_since_bullet += 1
	if frames_since_bullet == 100:
		spawn_bullet()
		frames_since_bullet = 0

func follow_player():
	direction = self.position - GlobalVars.player_pos
	set_rotation(direction)
	self.position -= direction.normalized() * SPEED

func set_rotation(dir):
	self.rotation = - atan2(dir.x, dir.y)

func spawn_bullet():
	var new_bullet = bullet.instance()
	new_bullet.direction = direction
	new_bullet.position = self.position - direction.normalized() * 50
	get_parent().add_child(new_bullet)

func die():
	GlobalVars.score += 1
	print("score: ", GlobalVars.score)
	get_parent().enemie_dead(self.position)
	queue_free()