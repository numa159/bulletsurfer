extends Control

var direction = Vector2()
var touch_pos = Vector2()
var touch_vector = Vector2()

signal on_action
onready var score = get_node("HBoxContainer/Score")
onready var hscore = get_node("HBoxContainer/HScore")

func _ready():
	hscore.text = str("HIGH SCORE: ", GlobalVars.high_score)

func _process(delta):
	var new_dir = (touch_vector - touch_pos).normalized()
	if new_dir != Vector2():
		direction = new_dir
	score.text = str("SCORE: ", GlobalVars.score)

func _on_Controls_gui_input(event):
	if event is InputEventScreenTouch:
		if event.index == 0:
			if event.is_pressed():
				touch_pos = event.position
				$BigCircle.position = touch_pos
				$SmallCircle.position = touch_pos
				$BigCircle.show()
				$SmallCircle.show()
			else:
				$BigCircle.hide()
				$SmallCircle.hide()
		else:
			emit_signal("on_action")
	elif event is InputEventScreenDrag:
		touch_vector = event.position
		$SmallCircle.position = touch_vector
	elif event is InputEventMouseButton and event.is_pressed():
		emit_signal("on_action")

func _unhandled_key_input(event):
	if event is InputEventKey and event.is_pressed():
		if event.scancode == 16777217:
			get_tree().change_scene("res://Scenes/Menu/MainMenu.tscn")
		else:
			emit_signal("on_action")