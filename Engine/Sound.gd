extends TextureButton

func _ready():
	if AudioServer.is_bus_mute(0):
		pressed = true
		AudioServer.set_bus_mute(0,true)
		modulate = Color(1,0,0,1)

func _on_Sound_toggled(button_pressed):
	if button_pressed:
		modulate = Color(1,0,0,1)
		AudioServer.set_bus_mute(0,true)
	else:
		modulate = Color(0,1,0,1)
		AudioServer.set_bus_mute(0,false)

func _on_Sound_gui_input(ev):
	if ev is InputEventScreenTouch and ev.is_pressed():
		pressed = not pressed
		_on_Sound_toggled(pressed)