extends KinematicBody2D

const TYPE = "Player"

var last_dir = Vector2(0,-1)
var direction = Vector2()
var speed = 5

var directional_controls

onready var effects = {
		"trail" : $TrailEffect,
		"boost" : $BoostEffect}

signal player_died
signal enemie_despawned

enum States {NORMAL, G_BUFF}
onready var states_dic = {
		NORMAL : NormalState.new(effects),
		G_BUFF : GreenBuffState.new(effects)}

onready var state_class = NormalState.new(effects)
var state = NORMAL

onready var controls = get_node("../UI/Controls")

#### FUNCIONALITY ####
func _init():
	GlobalVars.player = self
	GlobalVars.game_over = false

func _ready():
	controls.connect("on_action", self, "action")
	set_state(0)
#	directional_controls = funcref(self, "touch_direction")
	if GlobalVars.operating_system == "Android":
		directional_controls = funcref(self, "touch_direction")
	else:
		directional_controls = funcref(self, "mouse_direction")

func _physics_process(delta):
	speed = state_class.speed
	directional_controls.call_func()

#### CONTROL SUPPORT ####
func touch_direction(): # Touch Support
	direction = -controls.direction
	if direction != Vector2():
		get_parent().position -= direction.normalized() * speed
		set_rotation(direction)
		last_dir = direction
	else:
		get_parent().position -= last_dir.normalized() * speed
		set_rotation(last_dir)
	GlobalVars.player_pos = get_parent().position

func mouse_direction(): # Mouse Support
	direction = get_parent().position - get_global_mouse_position()
	set_rotation(direction)
	get_parent().position -= direction.normalized() * speed
	GlobalVars.player_pos = get_parent().position

#### HELPER FUNCTIONS ####
func set_rotation(dir):
	self.rotation = - atan2(dir.x, dir.y)

func set_materials(mat):
	$Sprite.set("material", mat)
	$TrailEffect.set("material", mat)
	$BoostEffect.set("material", mat)
	print("set")

func die():
	GlobalVars.game_over = true
	get_node("BulletGravityArea").call_deferred("set_monitoring", false)
	get_node("EnemieSpawnArea").call_deferred("set_monitoring", false)
	get_node("Collision").call_deferred("set_monitoring", false)
	emit_signal("player_died")

#### BULLET GRAVITY ####
func _on_BulletGravityArea_body_entered(body):
	if body.TYPE == "Bullet":
		body.following = true

func _on_BulletGravityArea_body_exited(body):
	if body.TYPE == "Bullet":
		body.following = false

#### GAME AREA ####
func _on_EnemieSpawnArea_body_exited(body):
	if body.TYPE == "Enemie":
		emit_signal("enemie_despawned")
		GlobalVars.enemie_count -= 1
	elif body.TYPE == "Buff":
		GlobalVars.buff_count -= 1
	if body is Particles2D:
		pass
	else:
		body.queue_free()

#### GENERIC FUNCTIONS ####
func action():
	state_class.action()

func _on_Collision_body_entered(body):
	state_class.body_entered(body)

#### STATE SWAP ####
func set_state(new_state=0):
	state_class.exit()
	state = new_state
	state_class = states_dic[new_state]
	state_class.initialize()

func get_state():
	return state

func set_temporary_state(new_state, time):
	set_state(new_state)
	yield(get_tree().create_timer(time), "timeout")
	set_state()

#### STATES ####

class NormalState:
	
	const NAT_SPEED = 5
	var speed = NAT_SPEED
	var boosting = false
	var trail_effect
	var boost_effect
	var norm_mat = preload("res://Engine/NormalMaterial.tres")
	var state_changed = false

	func _init(effects):
		trail_effect = effects["trail"]
		boost_effect = effects["boost"]
	
	func initialize():
		boosting = false
		speed = NAT_SPEED
		state_changed = false
		trail_effect.emitting = true
		boost_effect.emitting = false
		GlobalVars.player.set("material", norm_mat)

	func action():
		if not boosting:
			speed *= NAT_SPEED * 2
			boosting = true
			trail_effect.emitting = false
			boost_effect.emitting = true
			var timer = Timer.new()
			timer.set_one_shot(false)
			timer.set_wait_time(0.1)
			timer.start()
			GlobalVars.add_child(timer)
			yield(timer, "timeout")
			timer.queue_free()
			if not state_changed:
				speed = NAT_SPEED
				boosting = false
				trail_effect.emitting = true
				boost_effect.emitting = false

	func body_entered(body):
		if not GlobalVars.game_over:
			if (body.TYPE == "Bullet" || body.TYPE == "Enemie") and not boosting:
				GlobalVars.player.die()
			elif body.TYPE == "Enemie":
				body.die()
			elif body.TYPE == "Bullet":
				body.queue_free()
			elif body.TYPE == "Buff":
				GlobalVars.player.set_temporary_state(body.BUFF_TYPE, body.BUFF_DURATION)
				body.die()
	
	func exit():
		state_changed = true

class GreenBuffState:
	
	const NAT_SPEED = 5
	var speed
	var boosting
	var trail_effect
	var boost_effect
	var norm_mat = preload("res://Engine/RainbowShader.tres")

	func _init(effects):
		trail_effect = effects["trail"]
		boost_effect = effects["boost"]

	func initialize():
		speed = NAT_SPEED * NAT_SPEED
		boosting = true
		trail_effect.emitting = false
		boost_effect.emitting = true
		GlobalVars.player.set("material", norm_mat)
	
	func action():
		pass

	func body_entered(body):
		if (body.TYPE == "Bullet" || body.TYPE == "Enemie") and not boosting:
			GlobalVars.player.die()
		elif body.TYPE == "Enemie":
			body.die()
		elif body.TYPE == "Bullet":
			body.queue_free()
	
	func exit():
		pass
